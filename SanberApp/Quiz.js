import React, { useEffect, useState } from 'react'
import { View, Text } from 'react-native'

const Quiz = () => {

    const [name, setName] = useState('John Doe')

    useEffect(() => {
        setTimeout(() => {
            setName('Zaky Muhammad Fajar')
        }, 2000)
        return () => {
            setName('Achmad Hilmy')
        }
    })

    return (
        <View>
            <Text>{name}</Text>
        </View>
    )
}

export default Quiz