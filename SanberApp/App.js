import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Quiz from './Quiz';
import Quiz3 from './Quiz3';
import Telegram from './Tugas/Tugas12/Telegram';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import RestApi from './Tugas/Tugas14/RestApi';
import TugasRoute from './Tugas/Tugas15/index'

export default function App() {
  return (
    //<Telegram />
    //<LoginScreen />
    //<AboutScreen />
    //<RestApi />
    //<TugasRoute />
    //<Quiz />
    <Quiz3 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
