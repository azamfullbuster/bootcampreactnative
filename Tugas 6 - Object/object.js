//soal no.1 - array to object
console.log("Soal 1 - Array to Object")
function arrayToObject(inputArr) {
    //get current year
    var now = new Date()
    var thisYear = now.getFullYear() //2021

    var biodata = [];

    //get every array items
    for (var i = 0; i < inputArr.length; i++) {
        //define array variable
        var arrFirstName = inputArr[i][0];
        var arrLastName = inputArr[i][1];
        var arrGender = inputArr[i][2];
        var arrBirthyear = inputArr[i][3];

        //check input parameter
        if (arrBirthyear == null || arrBirthyear > thisYear) {
            if (arrGender == null) {
                if (arrLastName == null) {
                    if (arrFirstName == null) {
                        var errMessage = "Empty Array";
                    }
                    else {
                        //add object to array
                        var bio = {};

                        bio.firstName = arrFirstName;
                        bio.lastName = " ";
                        bio.gender = "Invalid Gender";
                        bio.age = "Invalid Birth Year";

                        biodata.push(bio);
                    }
                }
                else {
                    //add object to array
                    var bio = {};

                    bio.firstName = arrFirstName;
                    bio.lastName = arrLastName;
                    bio.gender = "Invalid Gender";
                    bio.age = "Invalid Birth Year";

                    biodata.push(bio);
                }
            }
            else {
                //add object to array
                var bio = {};

                bio.firstName = arrFirstName;
                bio.lastName = arrLastName;
                bio.gender = arrGender;
                bio.age = "Invalid Birth Year";

                biodata.push(bio);
            }
        }
        else {
            //add object to array
            var bio = {};

            bio.firstName = arrFirstName;
            bio.lastName = arrLastName;
            bio.gender = arrGender;
            bio.age = thisYear - arrBirthyear;

            biodata.push(bio);
        }
    }

    //output array of object
    if (biodata.length > 0) {
        var no = 1;
        biodata.forEach(function (item) {
            console.log(no + ". " + item.firstName + " " + item.lastName + " : { " + "firstName:" + ` "` + item.firstName + `", ` + "lastName:" + ` "` + item.lastName + `", ` + "gender:" + ` "` + item.gender + `", ` + "age: " + item.age + "}");
            no++;
        });
        console.log("\n")
    }
    else {
        console.log(`" "`)
        console.log("\n")
    }
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff"]]
arrayToObject(people)

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)

arrayToObject([])


//Soal no.2 - Shopping time
console.log("Soal 2 - Shopping Time")

function shoppingTime(memberID, money) {
    //define item list
    var items = [
        {
            "name": "Sepatu Stacattu",
            "price": 1500000
        },
        {
            "name": "Baju Zoro",
            "price": 500000
        },
        {
            "name": "Baju H&N",
            "price": 250000
        },
        {
            "name": "Sweater Uniklooh",
            "price": 175000
        },
        {
            "name": "Casing Handphone",
            "price": 50000
        },
    ]

    //parameter checking
    if (memberID == null || memberID == '') {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    }
    else {
        //define shoppingdetail object
        var shoppingDetail = {};
        var currentMoney = money;
        shoppingDetail.memberID = memberID;
        shoppingDetail.money = money;
        shoppingDetail.listPurchased = [];

        //get every items and calculate the shopping
        items.forEach(function (item) {
            if (currentMoney >= 50000) {
                if (currentMoney >= item.price) {
                    currentMoney = currentMoney - item.price;
                    shoppingDetail.listPurchased.push(item.name);
                }
            }
        });
        shoppingDetail.changeMoney = currentMoney;

        return shoppingDetail;
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());
console.log(shoppingTime('JdhweRxa53', 50000));


//Soal no. 3 - Naik Angkot
console.log("\nSoal 3 - Naik Angkot")
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var angkotDetail = [];

    //get every array items
    for (var i = 0; i < arrPenumpang.length; i++) {
        //define the parameter
        var penumpang = arrPenumpang[i][0];
        var from = arrPenumpang[i][1];
        var to = arrPenumpang[i][2];

        if (to == null || to == '') {
            return "[]";
        }
        else if (rute.includes(from) && rute.includes(to)) {
            //calculate the price
            var start = rute.indexOf(from);
            var finish = rute.indexOf(to);
            var price = 2000 * (finish - start);

            //store to object and push it to array
            var detail = {};
            detail.penumpang = penumpang;
            detail.naikDari = from;
            detail.tujuan = to;
            detail.bayar = price;

            angkotDetail.push(detail);
        }
        else {
            return "Rute doesn't exist"
        }
    }

    return angkotDetail;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B'], ['Megi', 'F', 'D']]));
console.log(naikAngkot([])); //[]