//====No 1 - Animal Class====
//release 0
class Animal {
    constructor(name) {
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }

    get name() {
        return this._name;
    }

    set name(x) {
        this._name = x;
    }

    get legs() {
        return this._legs;
    }

    set legs(x) {
        this._legs = x;
    }

    get cold_blooded() {
        return this._cold_blooded;
    }

    set cold_blooded(x) {
        this._cold_blooded = x;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//release 1
class Ape extends Animal {
    constructor(name, cold_blooded) {
        super(name, cold_blooded);
        this._legs = 2;
    }

    get legs() {
        return this._legs;
    }

    set legs(x) {
        this._legs = x;
    }

    yell() {
        console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(name, legs, cold_blooded) {
        super(name, legs, cold_blooded);
    }

    jump() {
        console.log("hop hop")
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

//====No 2 - Function to Class
class Clock {
    constructor({ template }) {
        this._template = template;
        this.timer;
    }

    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this._template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer)
    }

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
        // this.timer = setInterval(this.render.bind(this), 1000);
    }

}

var clock = new Clock({ template: 'h:m:s' });
clock.start(); 