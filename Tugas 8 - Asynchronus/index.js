let readBooks = require('./callback.js')

let books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]



//function untuk memanggi readBooks
let index = 0;
let pointer = 1;

function read(time, book) {
    readBooks(time, book, function (timeRemain) {
        if (index < books.length - 1) {
            read(timeRemain, books[0 + pointer])
            index++
            pointer++
        }
        else {
            console.log("Buku untuk dibaca telah habis")
        }
    })
}

read(10000, books[0]);
