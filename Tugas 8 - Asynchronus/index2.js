let readBooksPromise = require('./promise.js')

let books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]
let index = 0;
let pointer = 1;

function read(time, book) {
    readBooksPromise(time, book)
        .then(function (timeRemain) {
            if (index < books.length - 1) {
                read(timeRemain, books[0 + pointer]);
                index++;
                pointer++;
            }
            else {
                console.log(`Buku yang bisa dibaca sudah habis`)
            }
        })
        .catch(function (timeRemain) {
            console.log(`Waktu yang tersisa tinggal ${timeRemain}`)
        })
}

read(10000, books[0]);