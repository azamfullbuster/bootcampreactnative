//soal 1 - looping while

var nomor = 2;
console.log("Soal 1 - Looping While")
while (nomor <= 20) {
    if (nomor == 2) {
        console.log("LOOPING PERTAMA");
        console.log(nomor + " - " + 'I love coding')
    }
    else {
        console.log(nomor + " - " + 'I love coding')
    }
    nomor += 2
}

while (nomor > 2) {
    nomor -= 2
    if (nomor == 20) {
        console.log("LOOPING KEDUA");
        console.log(nomor + " - " + 'I will become a mobile developer')
    }
    else {
        console.log(nomor + " - " + 'I will become a mobile developer')
    }
}

//soal 2 - looping for
console.log("\nSoal 2 - Looping For")
var nomor = 0;
for (var deret = 0; deret < 20; deret++) {
    nomor++;
    if (nomor % 2 == 0) {
        console.log(nomor + " - " + "Berkualitas")
    } else if (nomor % 2 != 0 && nomor % 3 == 0) {
        console.log(nomor + " - " + "I love coding")
    } else {
        console.log(nomor + " - " + "Santai")
    }
}

//soal 3 - membuat persegi panjang #
console.log("\nSoal 3 - Membuat Persegi Panjang #")
var tinggi = 4;
var alas = 8;
var output = "";
for (var x = 0; x < tinggi; x++) {
    for (var y = 0; y < alas; y++) {
        output += "#";
    }
    console.log(output)
    output = '';
}

//soal 4 - membuat tangga
var tinggi2 = 7;
var output2 = "";
console.log("\nSoal 4 - Membuat Tangga")
for (var x = 0; x <= tinggi2; x++) {
    for (var y = 0; y < x; y++) {
        output2 += "#";
    }
    console.log(output2)
    output2 = '';
}

//soal 5 - membuat papan catur
var sisi = 8;
var output3 = "";
console.log("\nSoal 5 - Membuat Papan Catur")
for (var x = 0; x < sisi; x++) {
    for (var y = 0; y < sisi; y++) {
        if (x % 2 == 0) {
            if (y % 2 == 0) {
                output3 += " ";
            }
            else {
                output3 += "#";
            }
        }
        else {
            if (y % 2 == 0) {
                output3 += "#";
            }
            else {
                output3 += " ";
            }
        }
    }
    console.log(output3)
    output3 = '';
}