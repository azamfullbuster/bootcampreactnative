//if-else
var nama = "Mantab";
var peran = "Striker";

if (nama == "" || nama == null) {
    console.log("Nama harus diisi!" + '\n')
}
else if (peran == "" || peran == null) {
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!" + '\n')
}
else {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    if (peran == "Penyihir" || peran == "penyihir") {
        console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!" + '\n')
    }
    else if (peran == "Guard" || peran == "guard") {
        console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf!" + '\n')
    }
    else if (peran == "Werewolf" || peran == "werewolf") {
        console.log("Halo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam!" + '\n')
    }
    else {
        console.log("Peran yang kamu pilih tidak tersedia !")
        console.log("Silahkan pilih salah satu diantara peran berikut : ")
        console.log("- Penyihir")
        console.log("- Guard")
        console.log("- Werewolf" + '\n')
    }
}


//switch case
var tanggal = 30; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 0; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1909; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

if (tanggal != null && bulan != null && tahun != null) {
    if (bulan != 2) {
        if (tanggal >= 1 && tanggal <= 31) {
            if (tahun >= 1900 && tahun <= 2200) {
                switch (bulan) {
                    case 1: { console.log(tanggal + ' Januari ' + tahun); break; }
                    case 3: { console.log(tanggal + ' Maret ' + tahun); break; }
                    case 4: { console.log(tanggal + ' April ' + tahun); break; }
                    case 5: { console.log(tanggal + ' Mei ' + tahun); break; }
                    case 6: { console.log(tanggal + ' Juni ' + tahun); break; }
                    case 7: { console.log(tanggal + ' Juli ' + tahun); break; }
                    case 8: { console.log(tanggal + ' Agustus ' + tahun); break; }
                    case 9: { console.log(tanggal + ' September ' + tahun); break; }
                    case 10: { console.log(tanggal + ' Oktober ' + tahun); break; }
                    case 11: { console.log(tanggal + ' November ' + tahun); break; }
                    case 12: { console.log(tanggal + ' Desember ' + tahun); break; }
                    default: { console.log('Bulan yang dimasukkan harus antara 1 - 12'); }
                }
            }
            else {
                console.log("Tahun yang dimasukkan harus antara range 1900 - 2200 !")
            }
        }
        else {
            console.log("Tanggal yang dimasukkan harus antara range 1 - 31 !")
        }
    }
    else {
        if (tanggal >= 1 && tanggal <= 29) {
            if (tahun >= 1900 && tahun <= 2200) {
                switch (bulan) {
                    case 2: { console.log(tanggal + ' Februari ' + tahun); break; }
                    default: { console.log('Bulan yang dimasukkan harus antara 1 - 12'); }
                }
            }
            else {
                console.log("Tahun yang dimasukkan harus antara range 1900 - 2200 !")
            }
        }
        else {
            console.log("Tanggal yang dimasukkan harus antara range 1 - 29 karena bulan Februari !")
        }
    }
}
else {
    console.log("Masukkan tanggal, bulan, dan tahun terlebih dahulu !")
}

