//no1 - mengubah function menjadiarrow function
console.log("Soal 1 - Mengubah function menjadi arrow function")
const golden = () => { console.log("this is golden!!") };

golden();

//no2 - menyederhanakan menjadi object literal di es6
console.log("\nSoal 2 - Menyederhanakan menjadi es6")
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName() {
            console.log(`${firstName} ${lastName}`)
            return
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

//no3 - destructuring
console.log("\nSoal 3 - Destructuring")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation);

//no4 - array spreading
console.log("\nSoal 4 - Array Spreading")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east];
//Driver Code
console.log(combined)

//no5 - template literals
console.log("\nSoal 5 - Template Literals")
const planet = "earth"
const view = "glass"

const before = `Lorem ${view} dolor sot amet, consectetur adipiscing elit, ${planet} do euismod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log(before) 